module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            main: {
                src: ['tmp/', 'dist/']
            },
            tmp: {
                src: ['tmp/']
            },
        },
        concat: {
            adblock: {
                options: {
                    separator: ';\n\n',
                },
                files: {
                    'dist/revolution-adblock.user.js': ['src/adblock/meta.js', 'tmp/adblock/adblock.min.js'],
                }
            },
            gpro: {
                options: {
                    separator: ';\n\n',
                },
                files: {
                    'dist/revolution.user.js': ['src/gpro/meta.js', 'tmp/gpro/middleware.min.js', 'tmp/gpro/connector.min.js', 'tmp/gpro/popup.min.js', 'tmp/gpro/main.min.js'],
                }
            }
        },
        uglify: {
            options: {
                beautify: false,
                mangle: true
            },
            adblock: {
                files: {
                    'tmp/adblock/adblock.min.js': ['tmp/adblock/adblock.js']
                }
            },
            gpro: {
                files: {
                    'tmp/gpro/middleware.min.js': ['src/gpro/middleware.js'],
                    'tmp/gpro/connector.min.js': ['src/gpro/connector.js'],
                    'tmp/gpro/popup.min.js': ['tmp/gpro/popup.js'],
                    'tmp/gpro/main.min.js': ['src/gpro/main.js']
                }
            }
        },
        cssmin: {
            gpro: {
                files: {
                    'tmp/gpro/popup.min.css': ['src/gpro/popup.css']
                }
            },
            adblock: {
                files: {
                    'tmp/adblock/adblock.min.css': ['src/adblock/adblock.css']
                }
            }
        },
        htmlmin: {
            gpro: {
                options: {
                    collapseWhitespace: true,
                    preserveLineBreaks: false,
                },
                files: {
                    'tmp/gpro/popup.min.html': ['src/gpro/popup.html']
                }
            }
        }
    })

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');

    grunt.registerTask('inject', 'Inject html and css in file', function (mode) {

        if (mode == 'adblock') {
            var css = grunt.file.read('tmp/adblock/adblock.min.css')
            var adblock = grunt.file.read('src/adblock/adblock.js')

            adblock = adblock.replace('#CSS#', css)
            grunt.file.write('tmp/adblock/adblock.js', adblock);
            grunt.log.ok('adblock.js updated.');
        } else {
            var css = grunt.file.read('tmp/gpro/popup.min.css')
            var html = grunt.file.read('tmp/gpro/popup.min.html')

            // patch version number
            var meta = grunt.file.read('src/gpro/meta.js')
            var match = /@version\s+([\d\.]+)/.exec(meta)
            html = html.replace('#VERSION#', match[1])

            var popup = grunt.file.read('src/gpro/popup.js')
            popup = popup.replace('#CSS#', css)
            popup = popup.replace('#HTML#', html)

            grunt.file.write('tmp/gpro/popup.js', popup);
            grunt.log.ok('popup updated.');
        }
    });

    grunt.registerTask('default', ['gpro', 'adblock']);
    grunt.registerTask('adblock', ['clean:tmp', 'cssmin:adblock', 'inject:adblock', 'uglify:adblock', 'concat:adblock', 'clean:tmp']);
    grunt.registerTask('gpro', ['clean:tmp', 'cssmin:gpro', 'htmlmin:gpro', 'inject:gpro', 'uglify:gpro', 'concat:gpro', 'clean:tmp']);
};