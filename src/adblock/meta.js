// ==UserScript==
// @name         Revolution Adblock
// @namespace    http://gpro-revolution.de/
// @version      0.1.7
// @author       csch0
// @match        https://gpro.net/*
// @match        https://www.gpro.net/*
// @noframes
// @downloadURL  https://bitbucket.org/csch0/userscript-revolution-tools/raw/master/dist/revolution-adblock.user.js
// @updateURL    https://bitbucket.org/csch0/userscript-revolution-tools/raw/master/dist/revolution-adblock.user.js
// @grant        GM_addStyle
// @run-at       document-start
// ==/UserScript==;