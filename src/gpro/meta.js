// ==UserScript==
// @name         Revolution
// @namespace    http://gpro-revolution.de/
// @version      0.6.1
// @author       csch0
// @match        https://gpro.net/*
// @match        https://www.gpro.net/*
// @noframes
// @require      https://code.jquery.com/jquery-3.3.1.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.1/clipboard.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/pako/1.0.6/pako_deflate.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/platform/1.3.5/platform.min.js
// @connect      gpro.net
// @connect      csch0.schedar.uberspace.de
// @downloadURL  https://bitbucket.org/csch0/userscript-revolution-tools/raw/master/dist/revolution.user.js
// @updateURL    https://bitbucket.org/csch0/userscript-revolution-tools/raw/master/dist/revolution.user.js
// @grant        GM_addStyle
// @grant        GM_info
// @grant        GM_xmlhttpRequest
// ==/UserScript==