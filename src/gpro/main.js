(function () {
    'use strict';

    // check if logged in
    var a = $('a[href*="UpdateProfile.asp"]')
    if (a.length > 0) {
        // init popup
        var popup = new GproPopup()
        // keyboard shortcut
        document.addEventListener('keyup', function (event) {
            if (event.altKey && event.ctrlKey) {
                if (event.keyCode == 69) {
                    popup.init()
                    popup.toggle()
                }
            }
        });
    }
})();