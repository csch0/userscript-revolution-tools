class GproMiddleware {

    constructor() {
        this.base_url = 'https://csch0.schedar.uberspace.de/api/'
        this.gpro_url = 'https://gpro.net/gb/'
        this.gpro_www_url = 'https://www.gpro.net/gb/'

        this.urls = {
            'calendar': ['Calendar.asp', 'calendar'],
            'driver_profile': ['DriverProfile.asp?id=', 'driver_profile/'],
            'economy_history': ['EconomyHistory.asp', 'economy_history'],
            'gpro': ['gpro.asp', 'gpro'],
            'manager_profile': ['ManagerProfile.asp?idm=', 'manager_profile/'],
            'negotiate_sponsor': ['NegotiateSponsor.asp?id=', 'negotiate_sponsor/'],
            'negotiations_overview': ['NegotiationsOverview.asp', 'negotiations_overview'],
            'practice': ['Qualify.asp', 'practice'],
            'qualify': ['Qualify.asp', 'qualify'],
            'qualify2': ['Qualify2.asp', 'qualify2'],
            'race_analyses': ['RaceAnalysis.asp', 'race_analyses'],
            'race_analysis': ['RaceAnalysis.asp', 'race_analysis'],
            'race_setup': ['RaceSetup.asp', 'race_setup'],
            'race_thread': ['WeatherForecast.asp', 'race_thread/'],
            'staff_and_facilities': ['StaffAndFacilities.asp', 'staff_and_facilities'],
            'supplier': ['Suppliers.asp', 'supplier'],
            'suppliers': ['Suppliers.asp', 'suppliers'],
            'tech_d_profile': ['TechDProfile.asp?id=', 'tech_d_profile/'],
            'testing': ['Testing.asp', 'testing'],
            'track_details': ['TrackDetails.asp?id=', 'track_details/'],
            'tracks': ['ViewTracks.asp', 'tracks'],
            'update_car': ['UpdateCar.asp', 'update_car'],
            'version': ['gpro.asp', 'version'],
            'weather_forecast_testing': ['Testing.asp', 'weather_forecast_testing'],
            'weather_forecast': ['WeatherForecast.asp', 'weather_forecast'],
        }
        // debug only
        // this.base_url = 'http://127.0.0.1:8000/api/'
    }

    fetch(key, json, www_gpro_net, callbacks) {
        // save url for later usage
        var url = (www_gpro_net ? this.gpro_www_url : this.gpro_url) + this.url(key, json)[0]
        console.log('fetch', key, url)

        // request started from gpro.net, to we are not on a cross domain context
        if (window.location.origin == 'https://gpro.net' || window.location.origin == 'https://www.gpro.net') {
            var xhr = new XMLHttpRequest()
            xhr.open('GET', url)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 2) {
                    if (callbacks['before'] !== undefined) {
                        callbacks['before']({
                            'url': url,
                            'status': xhr.status
                        })
                    }
                }
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        if (callbacks['after'] !== undefined) {
                            callbacks['after']({
                                'url': url,
                                'status': xhr.status,
                                'html': xhr.response
                            })
                        }
                    } else {
                        console.log('error')
                        if (callbacks['error'] !== undefined) {
                            callbacks['error']({
                                'url': url,
                                'status': xhr.status
                            })
                        }
                    }
                }
            }
            xhr.send()
        } else {
            GM_xmlhttpRequest({
                method: 'GET',
                url: url,
                onerror: function (xhr) {
                    if (callbacks['error'] !== undefined) {
                        callbacks['error']({
                            'url': url,
                            'status': xhr.status
                        })
                    }
                },
                onreadystatechange: function (xhr) {
                    if (xhr.readyState == 2) {
                        if (callbacks['before'] !== undefined) {
                            callbacks['before']({
                                'url': url,
                                'status': xhr.status
                            })
                        }
                    }
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            if (callbacks['after'] !== undefined) {
                                callbacks['after']({
                                    'url': url,
                                    'status': xhr.status,
                                    'html': xhr.response
                                })
                            }
                        }
                    }
                }
            })
        }
    }

    parse(key, json, html, callbacks) {
        // save url for later usage
        var url = this.base_url + 'extension' + '/' + this.url(key, json)[1] + '/'
        console.log('parse', key, url)

        GM_xmlhttpRequest({
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            data: JSON.stringify({
                'id': key == 'gpro' ? 0 : json['gpro']['manager']['id'],
                'html': pako.deflate(html, {
                    level: 9,
                    to: 'string'
                })
            }),
            responseType: 'json',
            onerror: function (xhr) {
                if (callbacks['error'] !== undefined) {
                    callbacks['error']({
                        'url': url,
                        'status': xhr.status
                    })
                }
            },
            onreadystatechange: function (xhr) {
                if (xhr.readyState == 2) {
                    if (callbacks['before'] !== undefined) {
                        callbacks['before']({
                            'url': url,
                            'status': xhr.status
                        })
                    }
                }
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        if (callbacks['after'] !== undefined) {
                            callbacks['after']({
                                'url': url,
                                'status': xhr.status,
                                'json': xhr.response
                            })
                        }
                    }
                }
            }
        })
    }

    timestamp(json, callbacks) {
        // save url for later usage
        var url = this.base_url + 'cron' + '/' + 'timestamp' + '/'
        console.log('timestamp', url)

        // console.log('timestamp', json)
        GM_xmlhttpRequest({
            method: 'POST',
            url: this.base_url + 'cron' + '/' + 'timestamp' + '/',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            data: JSON.stringify({
                'json': JSON.stringify(json)
            }),
            responseType: 'json',
            onerror: function (xhr) {
                if (callbacks['error'] !== undefined) {
                    callbacks['error']({
                        'url': url,
                        'status': xhr.status
                    })
                }
            },
            onreadystatechange: function (xhr) {
                if (xhr.status == 200) {
                    if (xhr.readyState == 2) {
                        if (callbacks['before'] !== undefined) {
                            callbacks['before']({
                                'url': url,
                                'status': xhr.status
                            })
                        }
                    }
                    if (xhr.readyState == 4) {
                        if (callbacks['after'] !== undefined) {
                            callbacks['after']({
                                'url': url,
                                'status': xhr.status,
                                'json': xhr.response
                            })
                        }
                    }
                }
            }
        })
    }

    url(key, json, www_gpro_net) {
        var s = this.urls[key].slice()
        if (key == 'driver_profile') {
            s[0] += json['gpro']['driver_id']
            s[1] += json['gpro']['driver_id']
        } else if (key == 'tech_d_profile') {
            s[0] += json['gpro']['director_id']
            s[1] += json['gpro']['director_id']
        } else if (key == 'manager_profile') {
            s[0] += json['gpro']['manager']['id']
            s[1] += json['gpro']['manager']['id']
        } else if (key == 'track_details') {
            s[0] += json['gpro']['track']['id']
            s[1] += json['gpro']['track']['id']
        } else if (key == 'race_thread') {
            s[1] += json['gpro']['track']['id']
        }
        return s;
    }
}