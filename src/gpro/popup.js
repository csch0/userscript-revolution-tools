class GproPopup {
    constructor() {
        var self = this

        // init css, html, js
        this.initCSS()
        this.initHTML()
        this.initJS()

        this.connector = new GproConnector({
            'before': function (details) {
                self.showStatus(details['url'])
            },
            'after': function (details) {
                // check the simple data
                if (self.connector.option) {
                    self.modal.find('input[type=checkbox][simple-data="' + self.connector.option + '"]').prop('checked', true)
                }

                // disable not available options
                for (var i = 0; i < self.connector.skip_options['complex-data'].length; i++) {
                    self.modal.find('input[complex-data][name=' + self.connector.skip_options['complex-data'][i] + ']').addClass('disabled')
                    self.modal.find('input[complex-data][name=' + self.connector.skip_options['complex-data'][i] + ']').prop('disabled', true)
                }

                for (var i = 0; i < self.connector.skip_options['list-data'].length; i++) {
                    self.modal.find('input[list-data][name=' + self.connector.skip_options['list-data'][i] + ']').addClass('disabled')
                    self.modal.find('input[list-data][name=' + self.connector.skip_options['list-data'][i] + ']').prop('disabled', true)
                }

                for (var i = 0; i < self.connector.skip_options['simple-data'].length; i++) {
                    self.modal.find('input[simple-data][name=' + self.connector.skip_options['simple-data'][i] + ']').addClass('disabled')
                    self.modal.find('input[simple-data][name=' + self.connector.skip_options['simple-data'][i] + ']').prop('disabled', true)
                }

                if (self.connector.data['list'] && self.connector.data['list']['tracks']) {
                    self.modal.find('[data-tab=extended] #list_data_tracks').show()
                    self.modal.find('[data-tab=extended] #list_data_tracks select').empty()

                    // build select list
                    for (var i = 0; i < self.connector.data['list']['tracks'].length; i++) {
                        var option = $('<option value="' + self.connector.data['list']['tracks'][i]['id'] + '">#' + self.connector.data['list']['tracks'][i]['id'] + ': ' + self.connector.data['list']['tracks'][i]['name'] + ' (' + self.connector.data['list']['tracks'][i]['location'] + ')</option>')
                        self.modal.find('[data-tab=extended] #list_data_tracks select').append(option)
                    }
                }

                if (self.connector.options.length == 0) {
                    self.enableInputs()
                    self.hideStatus()
                }
                self.print()

            },
            'error': function (xhr, error) {
                self.showStatus(error['text'], true)
                if (self.connector.data['validator'] !== undefined) {
                    self.print()
                }
            }
        })
    }

    init() {
        if (!this.connector.initialised) {
            this.connector.init()
        }
    }

    initCSS() {
        GM_addStyle('#CSS#') // will be replaced by grunt
    }

    initHTML() {
        this.modal = $('<div id="gpro-revolution-extension" class="modal"></div>')
        this.modal.append($('#HTML#')) // will be replaced by grunt

        $('body').append(this.modal)
        $('#managersonline').append($('<li><a id="gpro-revolution-extension-button" href="#nogo">Team Extension</a></li>'))

        var self = this
        window.onclick = function (event) {
            if (event.target == self.modal[0]) {
                self.modal.hide()
            }
        }
    }

    initJS() {
        var self = this

        $('#gpro-revolution-extension-button').on('click', function () {
            self.init()
            self.show()
            return false
        })

        this.modal.find('a[data-tab]').on('click', function () {
            self.modal.find('div[data-tab]').hide()
            self.modal.find('div[data-tab=' + $(this).data('tab') + ']').show()
        })

        this.modal.find('input[name=format]').on('change', function () {
            self.print()
        })

        this.modal.find('input[name=clean]').on('click', function () {
            self.connector.reset()

            self.activateInputs()
            self.deselectInputs()
            self.enableInputs()

            self.modal.find('[data-tab=extended] #list_data_tracks').hide()
            self.modal.find('[data-tab=extended] #list_data_tracks select').empty()

            self.print()
        })

        var clipboard = new ClipboardJS(this.modal.find('input[name=copy]')[0])
        clipboard.on('success', function (e) {
            if (e.text) {
                self.showStatus('Copied to clipboard.')
            } else {
                self.showStatus('Nothing to copy to clipboard.')
            }
            self.hideStatus(2000)
            e.clearSelection()
        })
        clipboard.on('error', function (e) {
            self.showStatus('Error while copy to clipboard.', true)
            self.hideStatus(2000)
            e.clearSelection()
        })

        this.modal.find('input[simple-data]').on('change', function () {
            var options = [$(this).attr('simple-data')]
            if (this.checked) {
                self.connector.add(options)
                self.disableInputs()
            } else {
                self.connector.remove(options)
                self.enableInputs()
            }
            self.print()
        })

        this.modal.find('input[complex-data]').on('change', function () {
            var options = $(this).attr('complex-data').split('|')
            if (this.checked) {
                self.connector.reset()
                self.connector.add(options, $(this).attr('profile'))
                self.modal.find('input[name=format][value=json]').prop("checked", true)

                self.deactivateInputs(this.name)
                self.deselectInputs(this.name)
                self.disableInputs()
            } else {
                self.connector.reset()

                self.activateInputs()
                self.deselectInputs(this.name)
                self.enableInputs()
            }
            self.print()
        })

        this.modal.find('input[list-data]').on('change', function () {
            // change output format
            self.modal.find('input[name=format][value=json]').prop("checked", true)

            if (this.checked) {
                var options = [$(this).attr('list-data')]
                self.connector.reset()
                self.connector.add(options, $(this).attr('profile'))

                $('[data-tab=extended] #list_data_' + this.name).show()
                $('[data-tab=extended] #list_data_' + this.name + ' select').empty()

                self.deactivateInputs(this.name)
                self.deselectInputs(this.name)
                self.disableInputs()
            } else {
                self.connector.reset()

                self.activateInputs()
                self.deselectInputs(this.name)
                self.enableInputs()

                $('[data-tab=extended] #list_data_' + this.name).hide()
                $('[data-tab=extended] #list_data_' + this.name + ' select').empty()
            }
            self.print()
        })

        this.modal.find('[data-tab=extended] #list_data_tracks input').on('click', function () {
            var options = []
            self.modal.find('[data-tab=extended] #list_data_tracks select option:checked').each(function () {
                var option = 'track_details_' + this.value
                options.push(option)

                // extend urls array in middleware
                var url = self.connector.middleware.urls['track_details'].slice()
                self.connector.middleware.urls[option] = [url[0] + this.value, url[1] + this.value]
            })

            self.connector.reset()
            self.connector.add(options, $(this).attr('profile'))
        })
    }

    activateInputs() {
        this.modal.find('input[type=checkbox]').removeClass('inactive')
    }

    deactivateInputs(name) {
        this.modal.find('input[type=checkbox][name!=' + name + ']').addClass('inactive')
    }

    deselectInputs(name) {
        this.modal.find('input[type=checkbox][name!=' + name + ']').prop('checked', false)
    }

    disableInputs() {
        this.modal.find('input:not(.disabled)').prop('disabled', true)
        this.modal.find('select:not(.disabled)').prop('disabled', true)
    }

    enableInputs() {
        this.modal.find('input:not(.disabled):not(.inactive)').prop('disabled', false)
        this.modal.find('select:not(.disabled):not(.inactive)').prop('disabled', false)
    }

    hide() {
        this.modal.hide()
    }

    show() {
        this.modal.show()
    }

    toggle() {
        this.modal.toggle()
    }

    showStatus(s, error) {
        this.modal.find('.status').show()
        this.modal.find('.status span').html(s.substr(0, 70))
        if (error) {
            this.modal.find('.status span').addClass('error')
        }
    }

    hideStatus(delay) {
        this.modal.find('.status').delay(delay !== undefined ? delay : 0).hide(0)
    }

    print() {
        if (this.connector.data['validator'] !== undefined && !this.connector.data['validator']['valid'] && this.connector.data['validator']['errors']) {
            var textarea = ''
            for (var i = 0; i < this.connector.data['validator']['errors'].length; i++) {
                if (textarea.length) {
                    textarea += '\n'
                }
                textarea += '[' + this.connector.data['validator']['errors'][i]['property'] + '] ' + this.connector.data['validator']['errors'][i]['message']
            }
            this.disableInputs()
        } else {
            if (this.modal.find('input[name=format]:checked').val() == 'json') {
                var textarea = JSON.stringify(this.connector.data['json'], null, 4)
            } else {
                var textarea = ''
                for (var i = 0; i < this.connector.data_order.length; i++) {
                    if (this.connector.data['bbcode'][this.connector.data_order[i]]) {
                        if (textarea.length) {
                            textarea += '\n\n'
                        }
                        textarea += this.connector.data['bbcode'][this.connector.data_order[i]]
                    }
                }

                if (this.connector.data['bbcode']['race_analyses']) {
                    for (var i = 0; i < this.connector.data['bbcode']['race_analyses'].length; i++) {
                        if (textarea.length) {
                            textarea += '\n\n'
                        }
                        textarea += this.connector.data['bbcode']['race_analyses'][i]
                    }
                }

                if (this.connector.data['bbcode']['tracks']) {
                    for (var i = 0; i < this.connector.data['bbcode']['tracks'].length; i++) {
                        if (textarea.length) {
                            textarea += '\n\n'
                        }
                        textarea += this.connector.data['bbcode']['tracks'][i]
                    }
                }
            }
        }
        this.modal.find('textarea').html(textarea)
    }
}