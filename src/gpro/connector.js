class GproConnector {

    constructor(callbacks) {
        this.callbacks = callbacks
        this.www_gpro_net = window.location.origin == 'https://www.gpro.net'

        this.initialised = false
        this.error = {
            'status': false,
            'text': null
        }

        this.data = {
            'json': {},
            'bbcode': {}
        }
        this.data_order = [
            'calendar',
            'driver_profile',
            'tech_d_profile',
            'update_car',
            'negotiations_overview',
            'staff_and_facilities',
            'practice',
            'qualify',
            'qualify2',
            'race_setup',
            'economy_history',
            'race_analysis',
            'race_analysis_extended',
            'race_thread',
            'suppliers',
            'thread',
            'track_details',
            'tracks_list',
            'testing',
            'weather',
            'weather_forecast'
        ]

        this.option = null
        this.options = []
        this.skip_options = {
            'complex-data': [],
            'list-data': [],
            'simple-data': []
        }

        this.middleware = new GproMiddleware(this.data)
    }

    init() {
        this.initialised = true
        this.option = 'gpro'
        this.init_fetch()
    }

    add(options, profile) {
        this.options = []
        for (var i = 0; i < options.length; i++) {
            if (this.skip_options['simple-data'].indexOf(options[i]) < 0) {
                this.options.push(options[i])
            }
        }

        if (profile !== undefined) {
            this.data['json']['profile'] = profile

            // add timestamp for database_* profiles
            if (profile.startsWith('database_', 0)) {
                this.options.push('timestamp')
            }

            if (profile == 'database_race_analyses') {
                this.data['bbcode']['race_analyses'] = []
                this.data['json']['race_analyses'] = []
            }

            if (profile == 'database_admin_tracks') {
                this.data['bbcode']['tracks'] = []
                this.data['json']['tracks'] = []
            }
        }

        this.option = this.options.shift()
        if (this.option) {
            this.fetch()
        }
    }

    remove(options) {
        for (var i = 0; i < options.length; i++) {
            delete this.data['bbcode'][options[i]]
            delete this.data['json'][options[i]]
        }
        if (this.callbacks['onRemove'] !== undefined) {
            this.callbacks['onRemove']()
        }
    }

    reset() {
        this.data = {
            'json': {
                'meta': this.data['json']['meta'],
                'gpro': this.data['json']['gpro']
            },
            'bbcode': {}
        }

        // delete timestamp if available
        if (this.data['json']['meta']['timestamp'] !== undefined) {
            delete this.data['json']['meta']['timestamp']
        }

        if (this.callbacks['onReset'] !== undefined) {
            this.callbacks['onReset']()
        }
    }

    init_fetch() {
        var self = this
        if (this.www_gpro_net) {
            this.middleware.fetch(this.option, this.data['json'], true, {
                'before': function (xhr) {
                    if (self.callbacks['before'] !== undefined) {
                        self.callbacks['before'](xhr)
                    }
                },
                'after': function (xhr) {
                    // check if account is logged in
                    if ($(xhr.html).find('a[href*="UpdateProfile.asp"]').length > 0) {
                        self.parse(xhr.html)
                    } else {
                        self.error['status'] = true
                        self.error['text'] = 'Please make sure you are logged in on gpro.net.'
                        if (self.callbacks['error'] !== undefined) {
                            self.callbacks['error'](xhr, self.error)
                        }
                    }
                },
                'error': function (xhr) {
                    if (window.location.origin == 'https://www.gpro.net') {
                        self.error['status'] = true
                        self.error['text'] = 'Unknown error while loading the data from gpro.net'
                        if (self.callbacks['error'] !== undefined) {
                            self.callbacks['error'](xhr, self.error)
                        }
                    } else {
                        self.www_gpro_net = false
                        self.init()
                    }
                }
            })
        } else {
            this.middleware.fetch(this.option, this.data['json'], false, {
                'before': function (xhr) {
                    if (self.callbacks['before'] !== undefined) {
                        self.callbacks['before'](xhr)
                    }
                },
                'after': function (xhr) {
                    // check if account is logged in
                    if ($(xhr.html).find('a[href*="UpdateProfile.asp"]').length > 0) {
                        self.parse(xhr.html)
                    } else {
                        self.error['status'] = true
                        self.error['text'] = 'Please make sure you are logged in on gpro.net.'
                        if (self.callbacks['error'] !== undefined) {
                            self.callbacks['error'](xhr, self.error)
                        }
                    }
                },
                'error': function (xhr) {
                    if (window.location.origin == 'https://gpro.net') {
                        self.error['status'] = true
                        self.error['text'] = 'Unknown error while loading the data from gpro.net'
                        if (self.callbacks['error'] !== undefined) {
                            self.callbacks['error'](xhr, self.error)
                        }

                    } else {
                        self.www_gpro_net = true
                        self.init()
                    }
                }
            })
        }
    }

    fetch(www_gpro_net = false) {
        var self = this
        this.middleware.fetch(this.option, this.data['json'], this.www_gpro_net, {
            'before': function (xhr) {
                if (self.callbacks['before'] !== undefined) {
                    self.callbacks['before'](xhr)
                }
            },
            'after': function (xhr) {
                // check if account is logged in
                if ($(xhr.html).find('a[href*="UpdateProfile.asp"]').length > 0) {
                    self.parse(xhr.html)
                } else {
                    self.error['status'] = true
                    self.error['text'] = 'Please make sure you are logged in on gpro.net.'
                    if (self.callbacks['error'] !== undefined) {
                        self.callbacks['error'](xhr, self.error)
                    }
                }
            },
            'error': function (xhr) {
                self.error['status'] = true
                self.error['text'] = 'Unknown error while loading the data from gpro.net'
                if (self.callbacks['error'] !== undefined) {
                    self.callbacks['error'](xhr, self.error)
                }
            }
        })
    }

    parse(html) {
        var self = this
        this.middleware.parse(this.option, this.data['json'], html, {
            'after': function (xhr) {
                // check if validator is present in json response
                if (xhr.json['validator'] && xhr.json['validator']['valid'] == false) {
                    // save validator in data
                    self.data['validator'] = xhr.json['validator']

                    self.error['status'] = true
                    self.error['text'] = 'Validator error, see output tab for more details.'
                    if (self.callbacks['error'] !== undefined) {
                        self.callbacks['error'](xhr, self.error)
                    }
                } else {

                    // meta data first to keep the right order on clean
                    if (self.option == 'gpro') {
                        self.data['json']['meta'] = {
                            'browser': platform.name,
                            'browser_version': platform.version,
                            'os': platform.os.toString(),
                            'script_version': GM_info.script.version,
                            'server_version': xhr.json['version']
                        }
                    }

                    if (self.data['json']['profile'] == 'database_race_analyses') {
                        self.data['bbcode']['race_analyses'].push(xhr.json['bbcode']['race_analysis'])
                        self.data['json']['race_analyses'].push(xhr.data['json']['race_analysis'])
                    }
                    //
                    else if (self.data['json']['profile'] == 'database_admin_tracks') {
                        self.data['bbcode']['tracks'].push(xhr.json['bbcode']['track_details'])
                        self.data['json']['tracks'].push(xhr.json['json']['track_details'])
                    }
                    //
                    else if (xhr.json['list'] !== undefined) {
                        self.data['list'] = xhr.json['list']
                    }
                    //
                    else {
                        // merge the data
                        jQuery.extend(self.data['bbcode'], xhr.json['bbcode'])
                        jQuery.extend(self.data['json'], xhr.json['json'])
                    }


                    if (self.option == 'gpro') {

                        // no supporter
                        if (self.data['json']['gpro']['manager']['supporter'] == false) {
                            self.skip_options['list-data'].push('race_analyses')
                        }

                        // no driver
                        if (self.data['json']['gpro']['driver_id'] < 1) {
                            self.skip_options['complex-data'].push('tools_setup_predictor')
                            self.skip_options['complex-data'].push('tools_testing_planner')
                            self.skip_options['complex-data'].push('tools_race_planner')

                            self.skip_options['simple-data'].push('driver_profile')
                            self.skip_options['simple-data'].push('practice')
                            self.skip_options['simple-data'].push('qualify')
                            self.skip_options['simple-data'].push('qualify2')
                            self.skip_options['simple-data'].push('race_setup')
                            self.skip_options['simple-data'].push('testing')
                        }

                        // no technical director
                        if (self.data['json']['gpro']['director_id'] < 1) {
                            self.skip_options['simple-data'].push('tech_d_profile')
                        }

                        // season reset
                        if (self.data['json']['gpro']['track']['race'] == 0) {
                            self.skip_options['complex-data'].push('database_before_season')
                            self.skip_options['complex-data'].push('database_before_race')
                            self.skip_options['complex-data'].push('tools_setup_predictor')
                            self.skip_options['complex-data'].push('tools_testing_planner')
                            self.skip_options['complex-data'].push('tools_race_planner')

                            self.skip_options['simple-data'].push('practice')
                            self.skip_options['simple-data'].push('qualify')
                            self.skip_options['simple-data'].push('qualify2')
                            self.skip_options['simple-data'].push('race_setup')
                            self.skip_options['simple-data'].push('testing')
                            self.skip_options['simple-data'].push('weather_forecast')
                            self.skip_options['simple-data'].push('track_details')
                        }

                        // first race
                        else if (self.data['json']['gpro']['track']['race'] == 1) {
                            self.skip_options['complex-data'].push('database_after_race')
                        }

                        // after first race
                        else if (self.data['json']['gpro']['track']['race'] > 1) {
                            self.skip_options['complex-data'].push('database_before_season')
                        }
                    }

                    if (self.callbacks['after'] !== undefined) {
                        self.callbacks['after'](xhr)
                    }

                    // check next requested option
                    self.option = self.options.shift()
                    if (self.option) {
                        if (self.option == 'timestamp') {
                            self.timestamp()
                        } else {
                            self.fetch()
                        }
                    }
                }
            },
            'error': function (xhr) {
                self.error['status'] = true
                self.error['text'] = 'Unknown error while converting the data from gpro.net'
                if (self.callbacks['error'] !== undefined) {
                    self.callbacks['error'](xhr, self.error)
                }
            }
        })
    }

    timestamp(xhr) {
        var self = this
        this.middleware.timestamp(this.data['json'], {
            'after': function (xhr) {
                // save timestamp in meta data
                self.data['json']['meta']['timestamp'] = xhr.json['timestamp']

                // call after callback again, to print the correct timestamp
                if (self.callbacks['after'] !== undefined) {
                    self.callbacks['after'](xhr)
                }
            }
        })
    }

    print() {
        if (this.callbacks['print'] !== undefined) {
            this.callbacks['print']()
        }
    }
}