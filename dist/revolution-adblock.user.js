// ==UserScript==
// @name         Revolution Adblock
// @namespace    http://gpro-revolution.de/
// @version      0.1.7
// @author       csch0
// @match        https://gpro.net/*
// @match        https://www.gpro.net/*
// @noframes
// @downloadURL  https://bitbucket.org/csch0/userscript-revolution-tools/raw/master/dist/revolution-adblock.user.js
// @updateURL    https://bitbucket.org/csch0/userscript-revolution-tools/raw/master/dist/revolution-adblock.user.js
// @grant        GM_addStyle
// @run-at       document-start
// ==/UserScript==;;

!function(){"use strict";GM_addStyle('body>div#outer>div.boxy{display:none!important}div#adContainer,div#dvAd72890,div#raceads{display:none!important}div.boxy[style*="height:100px;"],div[style*="z-index: 9999;"]{display:none!important}')}();